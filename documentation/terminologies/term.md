## Terminologies 

[Back to homepage](../homepage/index.md)

**Arduino :** 
-  An Arduino is a small, programmable microcontroller board. It consists of a simple hardware platform and a development environment for writing software to control it. Essentially, it's a tiny computer that can sense and control objects in the physical world. You can use it to create interactive electronic devices, prototypes, and various DIY projects. Arduino is popular among hobbyists, students, and professionals alike due to its affordability, ease of use, and versatility.

    ![](uno.jpg)

**GLOF :** 
- A Glacier Lake Outburst Flood (GLOF) is a type of flood that occurs when water contained by a glacier or a glacier-dammed lake is suddenly released. These events typically happen when the natural dam holding the water back—often made of ice or rock debris—fails or is breached, causing a rapid discharge of water downstream. 

**Geographical infromation system:**
- A Geographic Information System (GIS) is a computer system that analyzes and displays geographically referenced information. It uses data that is attached to a unique location.

**Cutting edge technology:**
- Any piece of technology that has new features, processes, software, or techniques. It represent the latest developments in IT, product, and software development, and it often have functions that can affect multiple industries.

A laser cutting machine

![](images.jpg)

**C++ (High-level programming language)**
- C++ is a cross-platform language that can be used to create high-performance applications. C++ was developed by Bjarne Stroustrup, as an extension to the C language. C++ gives programmers a high level of control over system resources and memory.
- It is used in system software, game development, embedded systems, scientific computing and high-performance applications.

****Hall Effect Sensor****
- Hall-effect sensors are integrated circuits that transduce magnetic fields to electrical signals with accuracy, consistency, and reliability.

****Fusion 360****
- Fusion 360 is a commercial computer-aided design, computer-aided manufacturing, computer-aided engineering and printed circuit board design software application, developed by Autodesk. It is available for Windows, macOS and web browser, with simplified applications available for Android and iOS.

****Propeller****
- Propeller is a device consisting of a hub fitted with blades that is made to turn rapidly by an engine and is used especially for propelling airplanes and ships.

![](propeller.jpg)



[Back to homepage](../homepage/index.md)
