# GLOF Detector

[Project idea](../projectidea/project.md)

[Team information](../team/team.md)

[Terminologies](../terminologies/term.md)

[Progress](../Progress%20Work/progress.md)

![](./ugyen.jpg)


## Why choose this project ## 

-  GLOF detection projects often utilize advanced technologies such as remote sensing, geographic information systems (GIS), and machine learning algorithms. By incorporating these technologies into our project, we can gain practical experience with cutting-edge tools and methods used in the field of environmental monitoring and analysis.

- Particularly in hilly areas, GLOFs pose serious hazards to ecosystems and communities of people. We can support initiatives to solve environmental issues and advance sustainable development goals, like disaster resilience and climate action, by working on a project pertaining to GLOF detection.


## Benefits from this project ##
- Bhutan being one of the countries located in the himalayan mountains where we always have to be aware of events like glacier lake outburst due rapid climate changes taking place due to industrialization.

![](image.jpg)

- Our project would be able to help civilizations living near water bodies, we would be able to stop major problems like floods whicih are one of the major natural disasters happening in Bhutan time to time and destroying homes.

- Our GLOF detector wouold be able to warn people about the upcoming disaster, our project would be helping many people to survive such hazadardeous events. People would be able to escape and it would save lives.


[project idea](../projectidea/project.md)

[team information](../team/team.md)

[terminologies](../terminologies/term.md)




