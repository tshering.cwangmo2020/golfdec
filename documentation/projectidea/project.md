# Project Idea

[Back to homepage](../homepage/index.md)

[Progress](../Progress%20Work/progress.md)

## About our project ##
- We plan to make a GLOF dectecting system, which would be able to arlam people about disaters before hand. We are planning to showcase how our dectector would help in such situations.

Obejective : List Materials and sketch Design

Project Name: GLOF Detector

Materials Required :

    1.Straw / Pipe
    2.Arduino Uno
    3.Motor
    4.Bowl
    5.LED
    6.Buzzer
    7.Plywood
    8.Cotton
    9.Wire
    10.Hall Effect Sensor
    11.Breadboard


## Rough sketch : ## 

![](./unnamed.png)

## Our plan ##
- We intend to use the fab lab as much as we can in designing our projects on tinkercad and fusion. We will be also creating our circuits online first and then would be implemeting in real life. We will use technologies such as the laser cutting machine and 3d printing machines. 

    
- We plan to work during our free times and after dinner, requesting our teachers for permisions.

## How will it work? ## 
We will use C++ to program our arduino where we will mechanically direct the arduino to control the motor. The motor will push the water in the bowl(which will be behind the plywood) into another bowl(lake) where the increase in the level of water will connect the incomplete circuit. Which will light the LED and buzzer then we will use another set of motors to redirect the water to the earlier bowl. 

## Converting our project to a 3d design 

Model design (fusion): 

Circuit design (tinkercad): 

![](./Screenshot%202024-04-05%20155737.png)


[Back to homepage](../homepage/index.md)