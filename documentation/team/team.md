# Team Infromation #

[Back to homepage](../homepage/index.md)

### Team Name : STUK ###

## Team Leader: Sangay Wangdi

![](./sangay.jpg)

- **Sangay Wangdi** is currently studying in garde 11 and is taking science as his main stream. He is currently 17 years old, he is from Pemagatshel, Dechheling. He is taking space and technology as his internship and has a lot of passion learning more about physics.

- **His hobbies** are to play football and learn science and math.

- He aspires to build this project as it is his dream. In the process, he aims to learn to program the arduino board. 

- His role is the programmer of our group, his role is to code the ardiuno and to look over the coding.


## Team Member : Kinley Yangchen

![](./kinley.jpg)

- **Kinley Yangchen** is currently studying in grade 11 and is taking science as her main stream. She is from Tashigang, Bidung. She is currently 15 years old. She is taking space and technology as her intership and has a lot of passion learning about it. 

- **Her hobbies** are to read books, listen to musics and playing basketball as well as football. 

- From this project, she aspires to delve more into programming specifically, Arduino. Also she is keen to learn about electronics along with 3d designing. 

- Her role is to look over all the project ideas and lead around, she is also the one resposible for to directing us and our project.



## Team Memeber : Tshering Choden Wangmo

![](./tcw.jpg)

- **Tshering Choden Wangmo** is currently studying in grade 11 and is taking science as her main stream. She is 16 years old and is from Tsirang, Menchena. She is taking agriculture as her internship and has likes to learn about histroy.

- **Her hobbies** are to read books, listen to music and dance. She also likes to learn about supernatural beings.

- She aspires to be able learn more about ardiuno codings and wants to learn more about how this project will work and to be able to implement in real life.

- Her role is to document everything we the team does and to contribute to the coding of the ardiuno.

## Team Member : Ugyen Tshering

![](./h.png)

- **Ugyen Tshering** is currently studying in grade 11 and is taking science as his main stream. He is 17 years and is from Bumthang, Ura. He is taking space and technology as his internship. He has  alot of passion to learn about Fab Lab.

- **His hobbies** are to play chess, baskateball and table tennis. He learns whatever he is interested in and he has learned desgning and is a passionate grahic designer.

- He aspires to learn more about designing from this project and is wants to learn more about graphic designing form this project.

- His role is to design and also help in documentation. The designing of virtual model and physical asethetics will be looked over by him.

[Back to homepage](../homepage/index.md)


