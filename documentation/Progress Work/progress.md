[Homepage](../homepage/index.md)

[Terminologies](../terminologies/term.md)

[Project Idea](../projectidea/project.md)

[Team members](../team/team.md)

## About our project ##
- We plan to make a GLOF dectecting system, which would be able to arlam people about disaters before hand. We are planning to showcase how our dectector would help in such situations.

## Our plan ##
- We intend to use the fab lab as much as we can in designing our projects on tinkercad and fusion. We will be also creating our circuits online first and then would be implemeting in real life. We will use technologies such as the laser cutting machine and 3d printing machines.
We plan to work during our free times and after dinner, requesting our teachers for permisions.

## Why choose this project ##
- The project offers us the oppurtunity to learn 3d designing through Fusion 360, circuit designing through Tinkercad, and coding language C++ through Tinkercad.
In the process we can also learn skills such as collaboration, communication, digital learning and documentain skills as well.

## Progress ##
***Learning Experience***

**Objectives :**
- i.Circuit Designing
- ii.Add information to project idea
- iii.Add new terminologies
- iv.3d Designing

**Progress:**
-  i.Tried connecting led to pushbutton
- ii.Changed the information, added new information and change grammar.
- iii.Added terminology for GIS, cutting edge technology and C++.
- iv.Tried 3d designing for project in Fusion360

![](./Screenshot%202024-04-05%20155737.png)



![](./Screenshot%202024-04-05%20161815.png)


## Progress ##
***Learning Experience***

### Main objective: ###
- Learning Fusion
- Trying fusion
- Making our circuit again

**In the progress of working:**
- Today we are learning how to design in fusion 360. Our designing lead Ugyen Tshering takes the initiative of designing.
![](lead.jpg)
- Trying to make the circuit which we are going to use in out project.
![](pro.jpg)

- The circuit of the LED lights would be lights in the reality which would be flickering during when the GLOF is dectecting.We made our circuits in wokwi (a digital platform, an online stimulator)
![](v.png)

- Added buzzer with the LED lights, when the dectector detects the GLOF with the flickering of LED lights, the buzzer will also buzz.
![](buzz.png)

- Continuing with the documentation, with every step being taken to achieve our goal.
![](doc.png)


## Progress ##
***Learning Experience***

### Main objective: ###
- Designing a box to keep our materials.

**In the progress of working:**
- We are currently designing a box where we would be keeping our components in. We are using fusion 360 to design our box and to laser print our box. Then we are going to attach our components together making a box to store our required materials which we will be using to make our arlamimg system.
![](ok.jpg)

## Progress ##
***Learning Experience***

### Main objective: ###
- To laser cut our box which we designed and to put or glue everything together.

The folowing is our design which our designing lead Ugyen Tshering has designed using fusion. 

![](1.png)
Right and left component

![](2.png)
Top and bottom component

![](3.png)
Back and front component

## Project Update: ##

Due to a one-week nature retreat and various scheduled programs, we experienced a two-week interruption in our work. Despite this, we have now successfully completed our project box and acquired all necessary materials for our glacier lake outbrust dectecting system.

![](box.jpg)


Additionally, we have switched to an ultrasonic sensor because we lacked the magnet required for the Hall effect sensor.

We are working on our circuit for the dectecting system for today, Kinley Yangchen is working on with the ultarsonic sensor as we earlier studied how to make our dectecting system using a hall effect sensor. Now we are learning and working on with the ultrasonic sensor.

![](ultra.jpg)

### Our Tiral Circuit ###
We have run alot of test and had a lot of error in our circuit, after a lot of trials and errors we have finally got our ciruit and we will impliment our circuit with everyting setup after mid term.

![](circuit.JPG)

We have almost completed our project around 78% and we are starting with our designs of our structures and buildings, we are done designing our propeller which we have to use for our water pumping system.

![](ugyent.JPG)

Continuing with our documentation, were we are constantly updating each big step and small steps we are taking.

![](tcw.JPG)

## Design of our propeller and Houses ##
- We have completed the making of our circuit and we are also making progress in making the designs of our model. Currently we are done the dedign of the houese which are going to be one of the main components to complete our project.

- We are done with the design of the propeller and the houses, here are the images of our designs;

![](propeller.png)
- propeller 

![](basechim.png)
- The base of our houses and top 

![](chim.png)
- windows (front and back)

![](chim2.png)
- windows (left and right)

## Final Project ##
- We have completed the connection and also added a relay to a water pump to pump the water. We have also finished the final display. It took us around 4 days to complete the final work since we have already had all the codes and plan on what to do so we didn't have much complications.

![](final.jpg)

### Updates we did:
- We replaced the propellar with water pump.
- used relay to help with the water pump.

### Acknowledgements ##
- We would firstly would like to thank Sir Anith for giving us enough time and guidance to complete this project, Ngawang Pemo Drukpa (XI) for helping us with the connections and background paintings. We would also like to thank Kinley Sonam (XI) and Tenzin Rabgay Zangmo(XI) for helping with the paintings and support.

- We would also like to thank DGI for providing us this opportunity to work on this STEAM Exibition, and also to Sir Norbu Wangchuk for providing and helping us get the materials required in our project completion. 
# Thank You
